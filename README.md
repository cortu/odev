## Makine epsilonu (machine epsilon) nedir?
Bilgisayar biliminde genel bir terim olan, kullandığımız bilgisayarlarin precision’ının ne kadar olacağını yani sayısal hesaplama sınırına makine epsilonu (machine epsilon) denir.

Rounding yani yuvarlama denilen kavram ise gerçek hayattaki reel bir sayının floating point number sistemindeki karşılığına denk gelen sayının belirlenmesine yarayan bir sabittir. Bir sayı sistemindeki yuvarlama işleminde ise karşılaşılabilecek en büyük nispi hataya makine epsilonu denir. Terimsel ifadesine baktığımızda bilgisayarımızda 1 sayısı ile bir sonraki en yakın reel sayı arasındaki farka makine epsilonu da denmektedir.

Makine epsilonu kısaca hesaplamalarımızda çıkan mutlak nispi doğru hata için üst sınıra denmektedir. Bilgisayarda epsilondan daha küçük sayıları hesaplayamayacağınızdan dolayı hatayı epsilonun altına düşürdüğümüzde en doğru sonuca yaklaşmış olursunuz. (upper bound on the absolute relative true error).